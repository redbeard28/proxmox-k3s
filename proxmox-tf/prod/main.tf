
module "k3s-cluster-masters" {
  source = "../modules/generic-cluster"

  clone_template_name = "ubuntu-2104-template"
  name_prefix = "k3s-master"
  ips = var.ips_masters


  sshkeys = <<EOF
${var.ssh_key}
EOF
  sockets = 2
  cores = 1
  gateway = var.gateway
  bridge = "vmbr0" # Bridge of the proxmox. May be different from yours
  storage_size = "32G" # size of the OS disk
  storage_pool = "local_SSD" # name of the storage in proxmox
  target_node = "pve1" # Name of the proxmox
  tags = "k3smaster"
}


module "k3s-cluster-workers" {
  source = "../modules/generic-cluster"

  clone_template_name = "ubuntu-2104-template"
  name_prefix = "k3s-node"
  ips = var.ips_workers


  sshkeys = <<EOF
${var.ssh_key}
EOF
  sockets = 2
  cores = 1
  gateway = var.gateway
  bridge = "vmbr0" # Bridge of the proxmox. May be different from yours
  storage_size = "32G" # size of the OS disk
  storage_pool = "local_SSD" # name of the storage in proxmox
  target_node = "pve1" # Name of the proxmox
  tags = "k3sworker"
}

