resource "proxmox_vm_qemu" "generic-vm" {
  count = length(var.ips)

  name = "${var.node_name}"
  desc = "generic terraform-created vm"
  target_node = var.target_node

  clone = var.clone_template_name

  cores = var.cores
  sockets = var.sockets
  memory = 2048

	disk {
		ssd = 0
		type = "scsi"
		storage = "hermes_data"
		size = "32G"
	}

  network {
    #id = 0
    model = "virtio"
    bridge = var.bridge
  }

  ssh_user = "ubuntu"

  os_type = "cloud-init"
  ipconfig0 = "ip=${var.main_ip}/24,gw=172.30.100.1"

  sshkeys = <<EOF
${var.sshkeys}
EOF
}
