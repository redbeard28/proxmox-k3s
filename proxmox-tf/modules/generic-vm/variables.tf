variable "node_name" {
  description = "Name for VM"
  type        = string
}

variable "main_ip" {
  description = "IP for vm"
  type        = string
}
variable "ips" {}
variable "clone_template_name" {}
variable "target_node" {
  description = "node to deploy on"
  type = string
}
variable "sshkeys" {
  description = "ssh keys to drop onto each vm"
  type = string
}

variable "cores" {
  description = "number of cores to give each vm"
  type = number
  default = 2
}
variable "bridge" {
  description = "bridge to use for network"
  type = string
  default = "vmbr0"
}
variable "sockets" {
  description = "Sockets avalaibles"
  default = 1
  type = number
}