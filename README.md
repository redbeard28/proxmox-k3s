# k3s + Rancher on Proxmox

## Purpose
Use terraform ans ansible to deploy a k3s cluster on Proxmox

## Terraform proxmox provider
pm_api_url = "https://XXX.XXX.XXX.XXX:8006/api2/json"
proxmox_passwd => password of the user
proxmox_user => user

## proxmox.tfvars
This file is in secrets/proxmox.tfvars
```yaml
# PROXMOX CREDENTIALS 
proxmox_passwd = "XXXXXXXXX"
proxmox_user = "terraform@pve"
ips = [
    "192.168.2.9",
    "192.168.2.10",
    "192.168.2.11",
    "192.168.2.12",
    "192.168.2.13"
  ]
gateway = "192.168.2"


```

## Lauch deployment
### Create VM template
```bash
wget wget http://cloud-images.ubuntu.com/releases/hirsute/release/ubuntu-21.04-server-cloudimg-amd64-disk-kvm.img
# Export whatever storage pool you want to use to hold your VM images. Mine just happens to be named hermes_data
export STORAGE_POOL=local_SSD
qm create 8000 --memory 2048 --net0 virtio,bridge=vmbr0 --name=ubuntu-2104-template

qm importdisk 8000 ubuntu-21.04-server-cloudimg-amd64-disk-kvm.img $STORAGE_POOL

qm set 8000 --scsihw virtio-scsi-pci --scsi0 ${STORAGE_POOL}:8000/vm-8000-disk-0.raw

qm set 8000 --ide2 $STORAGE_POOL:cloudinit

qm set 8000 --serial0 socket --vga serial0

qm template 8000


```

Please note **--name=ubuntu-2104-template** is used for **var.clone_template_name** terraform var


## Ansible part
Please modify inventory.toml

## Kubectl on your desktop
Please add this line in your **~/.bashrc**
```bash
export KUBECONFIG=~/.kube/config:~/.kube/k3s.yaml
```

## Add some instruction here !!
